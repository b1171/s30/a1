const express = require('express');
const app = express();
const PORT = 4000;
const mongoose = require('mongoose');

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//connect server to mongodb atlas
mongoose.connect('mongodb+srv://admin:admin-1996%2F%2A@batch139.ppmgo.mongodb.net/users?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//get notification if successfully connected or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));

//Schema
const userSchema = new mongoose.Schema(
	{
		username: String,
		password: String
	}
);

//Models
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {

		let newUser = new User({
			"username": req.body.username,
			"password": req.body.password
		});

		newUser.save((err, savedUser) => {
			if(err){
				return res.send('error');
			} else {
				return res.send(`${req.body.username} added to users.`);
			}
		});	
});

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));
